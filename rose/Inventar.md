## Was Rose mitnimmt
- OG: kleines Bett
- OG: Spielgelkommode + Schrank (Mari Zimmer)
- OG: Maris Schreibtisch
- OG: Bettkasten (Meli Zimmer)
- OG: Klavier
- EG: Sekretär/Schänkchen/Eckschränkchen
- EG: Sessel und kleines Sofa
- EG: großer Wohnzimmer-Tisch
- EG: 2x WZ-Schrankeinheiten (ohne Mittelteil)
- EG: 4 Stühle
- EG: Geschirrschrank 2m -> Keller
- EG: Gardarobe + Schränkchen
- EG: Notenständer
- KG: Schreibtisch Michi
- KG: kleines Herbert-Bücherregal 160m hoch

## Was weg muss
- OG: Raumteiler (Meli Zimmer)
- OG: Bett (Meli Zimmer)
- OG: Billigbücherregal (Mari Zimmer)
- OG: Schlafzimmerschränke (70er Jahre)
	- 1xSpielgelkommode
	- 1xDoppelschrank 
	- 1xEinzelschrank
	- 1xBett
	- 2xNachttisch
- OG: Schaukelstuhl mit Fell (Rose Zimmer)
- OG: Schubladenschrank (Flur)
- OG: Notenregal, selbstbau, beige (Meli Zimmer)
- EG: großes Sofa
- EG: kleiner Wohnzimmer-Tisch
- EG: Esszimmertisch, ausziehbar
- KG: Schubladenschrank mit 4 Schubladen grün (Flur)
- KG: 2 Esszimmerstühle (von insgesamt 6)
- KG: Liegesessel Leder + Fußhocker (Michis Zimmer)
- KG: Raumteiler Michi inkl. Bücherregal
- KG: Canpingtisch, klappbar (Michis Zimmer)
- KG: Sauna/Wärmekabiene (bleibt ggf.)
- KG: Globus (Michis Zimmer)
- KG: 2xSessel mit Samtbezug (Adlys Zimmer)
- KG: Höhenverstellbarer WZ-Tisch, ausziehbar 124x65 (Adly Zimmer)
- KG: Schreibtisch 162x77(Adly Zimmer)
- KG: Bett (Adly Zimmer)
- KG: Schreibtischstuhl Lederbezug (Adly Zimmer)
- KG: Liegesessel mit Fußhocker (Adly Zimmer)
- KG: Wandbücherregal 2-reihig, schwarer Rahmen, 7 Bretter
- KG: Mikroskop (Adly Zimmer)
- KG: Plastikbank

## Mari
- KG: großes Herbert-Bücherregal 200m hoch

## Meli
- KG: Tisch Heizraum 109.5x79.5(Eiche, ausklappbar)
- KG: große Herbert-Bücherregal 189x100x24.5
- Gartenmöbel: wieviel/welche?

## Michi
- EG: Cello

## Laura
- KG: Schlafcouch Adly mit Samtbezug


## Ebay-Kleinanzeigen
- OG: Schlafzimmer komplett (450 VHB?)
- KG: Antiq-Buffet & Kredenz 1937, Massiv (Adly Zimmer)

## Neue Arbeit (Lahr/Hausach)
- KG: Nachttisch (Michis Zimmer)

## Sperrmüll
- KG: Bett Michi
- OG: Sessel (Mari Zimmer)
- OG: Sessel (Bad)
- OG: Holzstuhl vom Plus (Flur)